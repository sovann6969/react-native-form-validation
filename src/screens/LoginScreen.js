import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image, TouchableOpacity, KeyboardAvoidingView
} from "react-native";

import { Container, Content, Icon, Button, Item, Input, Label, Subtitle, } from 'native-base'
import CustomHeader from '../header/CustomHeader'


class SettingScreen extends Component {

    constructor(props) {
        super(props)

        this.state = {
            name: "",
            password: '',
            checkedName: null,
            checkedPass: null,
            msg: '',
            msgp: ''
        }
    }

    submitData = () => {

        const reg = /[^(\d|\w)+$]/



        if (this.state.name == "") {
            this.setState({
                checkedName: true,
                msg: "សូមបំពេញឈ្មោះ"
            })
        } else if (reg.exec(this.state.name)) {

            this.setState({
                checkedName: true,
                msg: "សូមបំពេញឈ្មោះដោយមិនមានសញ្ញាពិសេស"
            })
        } else if (this.state.password == "") {
            this.setState({
                checkedPass: true,
                msgp: "សូមបំពេញលេខសម្ងាត់"
            })
        } else if (this.state.password.length <= 6) {
            this.setState({
                checkedPass: true,
                msgp: "សូមបំពេញលេខសម្ងាត់លើសពី៦ខ្ទង់"
            })
        } else {
            this.props.navigation.navigate('HomeScreen', { name: this.state.name })
            this.setState({
                name: "",
                password: "", 
                checkedName: null,
                checkedPass: null,
            })
        }
    }
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: "ចូលប្រព័ន្ធ",
            headerLeft: () => (
                <Item onPress={() => navigation.navigate('Home')}><Icon name="arrow-back" style={{ paddingLeft: 10 }} /></Item>
            ),
        };
    };
    render() {
        return (
            <Container>

                <Content contentContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: -100 }}>
                    <Image style={{ width: 100, height: 100, borderRadius: 5 }} source={{ uri: "https://image.freepik.com/free-vector/welcome-composition-with-flat-character_23-2147895814.jpg" }}></Image>
                    <Text style={{ color: "purple" }}>
                        សូមស្វាគមន៍មកកា់កម្មវិធីយើង
                    </Text>

                    <Item floatingLabel>
                        <Label>នាម គោត្តនាម</Label>

                        <Input value={this.state.name}
                            onChangeText={val => this.setState({ name: val })} />

                    </Item >
                    {this.state.checkedName ? <Label style={{ color: 'red' }}>{this.state.msg}</Label> : null}
                    <Item style={{ marginTop: 10 }} floatingLabel>
                        <Label>លេខសំងាត់</Label>

                        <Input secureTextEntry={true} onChangeText={val => this.setState({ password: val })} />

                    </Item>
                    {this.state.checkedPass ? <Label style={{ color: 'red' }}>{this.state.msgp}</Label> : null}
                    <Button onPress={() => { this.submitData() }} style={{ backgroundColor: 'purple', marginTop: 10, width: '100%', borderRadius: 5, justifyContent: 'center' }}><Text style={{ color: 'white' }}> ចូលប្រព័ន្ធ </Text></Button>
                    <Label>
                        <Label> មានគណនីរួចហើយ ?</Label>
                        <Label onPress={() => { this.props.navigation.navigate("AddScreen") }}>ចុះឈ្មោះនៅទីនេះ</Label>

                    </Label>

                </Content>
            </Container>
        )
    }
}

export default SettingScreen
