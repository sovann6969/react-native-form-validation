import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image, KeyboardAvoidingView
} from "react-native";

import { Button, Container, Header, Content, Left, Item, Icon, Input, Form, Picker, Label } from 'native-base'



class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected2: undefined,
            name: '',
            pw: '',
            email: '',
            chName: null,
            cheEmail: null,
            chPw: null,
            msgp: '',
            msgn: '',
            msge: ''
        };
    }
    onValueChange2(value) {
        this.setState({
            selected2: value
        });
    }
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: "ចុះឈ្មោះ",
            headerLeft: () => (
                <Item onPress={() => navigation.goBack(null)}><Icon name="arrow-back" fontSize="20" style={{ paddingLeft: 10 }} /></Item>
            ),

        };
    };

    onSubmitData = () => {
        const reg = /[^(\d|\w)+$]/
        const regE = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
        if (this.state.name == "") {
            this.setState({
                chName: true,
                msgn: "សូមបំពេញឈ្មោះ"
            })
        } else if (reg.exec(this.state.name)) {

            this.setState({
                chName: true,
                msgn: "សូមបំពេញឈ្មោះដោយមិនមានសញ្ញាពិសេស"
            })
        } else if (this.state.pw == "") {
            this.setState({
                chPw: true,
                msgp: "សូមបំពេញលេខសម្ងាត់"
            })
        } else if (this.state.pw.length <= 6) {
            this.setState({
                chPw: true,
                msgp: "សូមបំពេញលេខសម្ងាត់លើសពី៦ខ្ទង់"
            })
        } else if (this.state.email) {
            this.setState({
                cheEmail: true,
                msge: "សូមបំពេញអ៊ីមែលអោយបានត្រឹមត្រូវ"
            })
        } else if (regE.exec(this.state.email)) {
            this.setState({
                cheEmail: true,
                msge: "សូមបំពេញអ៊ីមែលអោយបានត្រឹមត្រូវ"
            })
        } else {
            this.props.navigation.navigate('HomeScreen', { name: this.state.name })
            this.setState({
                name: "",
                pw: "",
                email: '',
                chName: null,
                cheEmail: null,
                chPw: null,
            })
        }
    }
    render() {
        return (

            <Container>
                <Content
                    contentContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: -100 }}>
                    <Image style={{ width: 100, height: 100, borderRadius: 5 }} source={{ uri: "https://image.freepik.com/free-vector/welcome-composition-with-flat-character_23-2147895814.jpg" }}></Image>
                    <Text style={{ color: "purple" }}>
                        សូមស្វាគមន៍មកកា់កម្មវិធីយើង
                    </Text>

                    <Item floatingLabel>
                        <Label>នាម គោត្តនាម</Label>

                        <Input value={this.state.name}
                            onChangeText={val => this.setState({ name: val })} />


                    </Item>
                    {this.state.chName ? <Label style={{ color: 'red' }}>{this.state.msgn}</Label> : null}
                    <Item floatingLabel>
                        <Label>អ៊ីមែល</Label>

                        <Input value={this.state.email}
                            onChangeText={val => this.setState({ email: val })} />

                    </Item>
                    {this.state.cheEmail ? <Label style={{ color: 'red' }}>{this.state.msge}</Label> : null}
                    <Item picker >
                        <Label>ថ្នាក់រៀន</Label>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined }}
                            placeholder="ថ្នាក់រៀន"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selected2}
                            onValueChange={this.onValueChange2.bind(this)}
                        >
                            <Picker.Item label="សៀមរាប" value="សៀមរាប" />
                            <Picker.Item label="បាត់ដំបង" value="បាត់ដំបង" />
                            <Picker.Item label="ភ្នំពេញ" value="ភ្នំពេញ" />
                            <Picker.Item label="កំពង់សោម" value="កំពង់សោម" />
                            <Picker.Item label="កំពង់ចាម" value="កំពង់ចាម" />
                        </Picker>
                    </Item>
                    <Item floatingLabel>
                        <Label>លេខសំងាត់</Label>

                        <Input secureTextEntry={true} value={this.state.pw}
                            onChangeText={val => this.setState({ pw: val })} />
                    </Item>
                    {this.state.chPw ? <Label style={{ color: 'red' }}>{this.state.msgp}</Label> : null}
                    <Button onPress={() => {
                        this.onSubmitData()
                    }} style={{ backgroundColor: 'purple', marginTop: 10, width: '100%', borderRadius: 5, justifyContent: 'center' }}><Text style={{ color: 'white' }}> Primary </Text></Button>
                    <Label>
                        <Label> មានគណនីរួចហើយ ?</Label>
                        <Label onPress={() => { this.props.navigation.navigate("Login") }}>ចុចចូលប្រព័ន្ធនៅទីនេះ</Label>

                    </Label>
                </Content>

            </Container>

        )
    }

}

export default HomeScreen;

const styles = StyleSheet.create({
    icon: {
        width: 24,
        height: 24,
    },
});