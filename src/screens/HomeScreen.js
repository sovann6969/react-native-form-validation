import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image
} from "react-native";
import { Button, Container, Header, Content, Left, Item, Icon } from 'native-base'

class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "ទំព័រដើម",
      headerLeft: () => (
        <Item onPress={() => navigation.openDrawer()}><Icon name="ios-menu" fontSize="20" style={{ paddingLeft: 10 }} /></Item>
      ),
      headerRight: () => (
        <Item onPress={() => navigation.navigate('Add')}><Icon name="add" style={{ paddingLeft: 10 }} /></Item>
      ),
    };
  };
  componentDidMount() {
    console.log("object")
  }
  render() {
    const { params } = this.props.navigation.state;
    const name = params ? params.name : '';

    return (
      <Container>
        <Content
          contentContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: -100 }}>
          <Image style={{ width: 250, height: 250, borderRadius: 5 }} source={{ uri: "https://image.freepik.com/free-vector/welcome-composition-with-flat-character_23-2147895814.jpg" }}></Image>
          <Text style={{ fontSize: 20 }}>
            <Text>សូមស្វាគមន៍:</Text>
            <Text style={{ color: 'purple' }}> {name}</Text>
          </Text>
        </Content>

      </Container>

    )
  }

}

export default HomeScreen;


const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },
});