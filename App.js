import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image
} from "react-native";

//library imports 
import { Container, Content, Icon, Header, Body } from 'native-base'
import {   createAppContainer} from 'react-navigation'
import { createDrawerNavigator,DrawerItems } from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack'

//custom files 
// import AppStackNavigator from './AppStackNavigator'
import LoginScreen from './src/screens/LoginScreen'
import HomeScreen from "./src/screens/HomeScreen";
import AddingForm from './src/screens/AddingForm'


export default class App extends React.Component{
  render(){
    return(
      <MyApp/>
    )
  }
}

const CustomDrawerContentComponent = (props) => (

  <Container>
    <Header >
      <Body>
      {/* <Icon name="ios-menu" style={{ paddingLeft: 10 }} onPress={() => navigation.navigate('DrawerOpen')} /> */}
      </Body>
    </Header>
    <Content>
      <DrawerItems {...props} />
    </Content>

  </Container>

);
const MyHomeStack = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen,
 
  },

 
})
const MyAddStack = createStackNavigator({
   AddScreen: {
    screen: AddingForm,

  },
})
const MyLoginStack = createStackNavigator({
  LoginScreen: {
    screen: LoginScreen,

  },
 
})
const MyDrawerNavigator = createDrawerNavigator({

  // For each screen that you can navigate to, create a new entry like this:
  Home:{
    screen:MyHomeStack,
 
    navigationOptions: () => 
    ({
        title: 'ទំព័រដើម',
        header:null
    }) 
  },
  Login:{
    screen:MyLoginStack,
 
    navigationOptions: () => 
    ({
        title: 'ចុចចូល',header: null 
    }) 
  },
  Add:{
    screen:MyAddStack,
 
    navigationOptions: () => 
    ({
        title: 'ចុះឈ្មោះ',header: null 
    }) 
  },



  },
  {
 backgroundColor:'white',
    initialRouteName: 'Home',
    drawerPosition: 'left',
    contentComponent: CustomDrawerContentComponent,
  
    
  });
 
  const MyApp = createAppContainer(MyDrawerNavigator);
const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  drawerHeader: {
    height: 200,
    backgroundColor: 'white'
  },
  drawerImage: {
    height: 150,
    width: 150,
    borderRadius: 75
  }

})
